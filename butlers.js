const exampleRequests = [
    {
        clientId: 1,
        requestId: 'abc',
        hours: 6
    },
    {
        clientId: 2,
        requestId: 'ghi',
        hours: 1
    },
    {
        clientId: 1,
        requestId: 'def',
        hours: 4
    },
    {
        clientId: 1,
        requestId: 'zzz',
        hours: 2
    },
    {
        clientId: 3,
        requestId: 'mmm',
        hours: 7
    }
]

const exampleReturnValue = {
    butlers: [
        {
            requests: ['abc', 'zzz']
        },
        {
            requests: ['def','ghi']
        }, 
        {
            etc: 'etc...'
        }
    ],
    spreadClientIds: [1,2]
}

function allocateAndReport(requests) {
   let exampleReturnValue = {butlers:[],spreadClientIds:[]};
   let butlerTotalHour = 8;
    requests.forEach((request,index) => {
        let requests = [];
        requests .push(request.requestId);
        if(request.hours <= butlerTotalHour){
            //if the butler hours are greator than request hours
            if(exampleReturnValue.butlers.length){
                exampleReturnValue.butlers[exampleReturnValue.butlers.length - 1].requests.push(request.requestId);
            }else{
                exampleReturnValue.butlers.push({requests:requests})
            }
            // exampleReturnValue.butlers.push({requests:requests});
            butlerTotalHour = butlerTotalHour-request.hours;
        } else if(request.hours >  butlerTotalHour){
            butlerTotalHour = 8;
            exampleReturnValue.butlers.push({requests:requests})





        }
         if (exampleReturnValue.spreadClientIds.indexOf(request.clientId)==-1) exampleReturnValue.spreadClientIds.push(request.clientId);
    });

    return exampleReturnValue;
}

console.log(JSON.stringify(allocateAndReport(exampleRequests)));